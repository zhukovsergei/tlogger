<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('reports', \App\Http\Controllers\Api\ReportController::class);

Route::controller(\App\Http\Controllers\Api\TaskController::class)->group(function () {
    Route::get('/tasks/get-by-id/{id}', 'getById');
    Route::post('/tasks/save-description/{taskId}', 'saveDescription');
});

Route::controller(\App\Http\Controllers\Api\ReportController::class)->group(function () {
    Route::post('/reports/show-by-date', 'showByDate');
    Route::get('/reports/search-task-list/{taskId}', 'searchTasks');
});

Route::controller(\App\Http\Controllers\Api\ScreenshotController::class)->group(function () {
    Route::get('/screenshot/get-by-task-id/{id}', 'getByTaskId');
    Route::post('/screenshot/upload-screenshot/{taskId}', 'uploadScreenshot');
    Route::delete('/screenshot/remove-id/{taskId}/{uuid}', 'removeMedia');
});


Route::controller(\App\Http\Controllers\Api\LinkController::class)->group(function () {
    Route::get('/link/get-all-by-task-id/{id}', 'getAllByTaskId');
    Route::post('/link/add-on-task-id/{id}', 'addOnTaskId');
    Route::delete('/link/remove-id/{id}', 'removeLink');
});
