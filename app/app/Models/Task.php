<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'description',
    ];

    public function reports()
    {
        return $this->hasMany(Report::class, 'task_id', 'id');
    }
}
