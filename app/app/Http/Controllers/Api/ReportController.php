<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReportResource;
use App\Models\Report;
use App\Models\Screenshot;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ReportResource
     */
    public function store(Request $request)
    {
        $data = $request->post('report');
        $report = Report::make($data);
        $report->date_on = $data['date_on'] ?? (new Carbon())->format('Y-m-d');
        $report->save();

        $report = Report::find($report->id);
        return new ReportResource($report);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ReportResource
     */
    public function show($id): ReportResource
    {
        $report = Report::findOrFail($id);

        return new ReportResource($report);
    }

    public function showByDate(Request $request): AnonymousResourceCollection
    {
        $report = Report::where('date_on', $request->get('date'))->get();

        return ReportResource::collection($report);
    }

    public function searchTasks(Request $request, $taskId): Collection
    {
        $taskList =  DB::table('reports')
            ->select('task_id')
            ->where('task_id', 'LIKE', '%'.$taskId.'%')->distinct()->get();

        return $taskList->pluck('task_id');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return ReportResource
     */
    public function update(Request $request, $id): ReportResource
    {
        $data = $request->post('report');
        $report = Report::findOrFail($id);

        $report->task_id = $data['task_id'];
        $report->text = $data['text'];
        $report->hours = $data['hours'];
        $report->update();

        $report = Report::find($id);
        return new ReportResource($report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return ReportResource
     */
    public function destroy($id): ReportResource
    {
        $report = Report::findOrFail($id);
        $report->delete();
        return new ReportResource($report);
    }
}
