<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ScreenshotResource;
use App\Models\Screenshot;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ScreenshotController extends Controller
{
    public function getByTaskId($id)
    {
        $screenshot = Screenshot::where('task_id', $id)->first();

       if(!$screenshot) {
            $screenshot = Screenshot::make(['task_id' => $id]);
            $screenshot->save();
        }

        return new ScreenshotResource($screenshot);
    }


    public function uploadScreenshot(Request $request, $taskId)
    {
        $screenshotFile = $request->file('screenshotFile');

        $newFilename = Str::uuid() . '.'. $screenshotFile->getClientOriginalExtension();

        $screenshot = Screenshot::where('task_id', $taskId)->first();
        $screenshot->addMedia($screenshotFile)->usingFileName($newFilename)->toMediaCollection();
        $screenshot->save();
        return new ScreenshotResource($screenshot);
    }

    public function removeMedia($taskId, $uuid)
    {
        $media = Media::where('uuid', $uuid)->first();
        $screenshot = Screenshot::where('task_id', $taskId)->first();
        $screenshot->deleteMedia($media->id);

        return new ScreenshotResource($screenshot);
    }

}
