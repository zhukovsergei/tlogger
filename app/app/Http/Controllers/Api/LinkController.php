<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\LinkResource;
use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function getAllByTaskId($id)
    {
        $links = Link::where('task_id', $id)->get();

        return LinkResource::collection($links);
    }

    public function addOnTaskId(Request $request, $id)
    {
        $link = $request->post('link');
        $link = Link::make(['text' => $link, 'task_id' => $id]);

        $link->save();

        return new LinkResource($link);
    }

    public function removeLink($id)
    {
        $link = Link::findOrFail($id);
        $link->delete();

        return new LinkResource($link);
    }


}
