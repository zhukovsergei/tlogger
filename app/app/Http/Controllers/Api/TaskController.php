<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;


class TaskController extends Controller
{
    public function getById($id): TaskResource
    {
        $task = Task::where('id', $id)->first();

        if(!$task) {
            $task = Task::make(['id' => $id]);
            $task->save();
        }
        
        $task = Task::where('id', $id)->first();
        return new TaskResource($task);
    }

    public function saveDescription(Request $request, $taskId): TaskResource
    {
        $description = $request->post('description');

        $task = Task::where('id', $taskId)->first();

        if(!$task) {
            $task = Task::make(['id' => $taskId, 'description' => $description]);
            $task->save();
        } else {
            $task->description = $description;
            $task->update();
        }

        return new TaskResource($task);
    }
}
