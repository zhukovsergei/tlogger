<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Report>
 */
class ReportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'date_on' => $this->faker->dateTimeBetween('-1 month', 'now'),
            'task_id' => $this->faker->numberBetween(100000, 200000),
            'text' => $this->faker->realTextBetween(100, 250),
            'hours' => $this->faker->numberBetween(1, 8),
        ];
    }


}
