<?php
use Illuminate\Support\Facades\Auth;
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>TLogger</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/app.js') }}" defer></script>
    <style>

    </style>
</head>
<body>
@if (Auth::check())
    <script>
        window.laravel = {!!json_encode([
            'isLogged' => true,
            'user' => Auth::user()
        ])!!}
    </script>
@else
    <script>
        window.laravel = {!!json_encode([
            'isLogged' => false
        ])!!}
    </script>
@endif
<div id="app">
    <Layout></Layout>
</div>
</body>

</html>
