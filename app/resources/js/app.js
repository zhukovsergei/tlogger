require('./bootstrap');
import moment from "moment";
import VueRouter from "vue-router";
import router from "./routes";
import VueClipboard from 'vue-clipboard2'
import Layout from "./Layout";

window.Vue = require('vue').default;

Vue.use(VueRouter);

VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);

router.beforeEach((to, from, next) => {
    if(to.name !== 'login' && !window.laravel.isLogged) {
        next({ name: 'login' });
    }
    next();
});

const app = new Vue({
    el: '#app',
    router,
    components: {
        'Layout': Layout
    },
    computed: {
        currentStringDate() {
            return moment().format('YYYY-MM-DD');
        }
    }
});
