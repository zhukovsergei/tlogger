import VueRouter from "vue-router";
import Reports from "./components/Reports.vue";
// import Record from "./components/Record.vue";
import Task from "./components/Task.vue";
import Login from "./auth/Login";


const routes = [
    {
        path: '/login',
        component: Login,
        name: 'login',
    },

    {
        path: '/:date?',
        component: Reports,
        name: 'home',
    },
/*    {
        path: '/record/:id',
        component: Record,
        name: 'record',
    },*/
    {
        path: '/task/:id',
        component: Task,
        name: 'task',
    },

    {
        path: '/logout',
        name: 'logout',
    },

];

const router = new VueRouter({
    mode: 'history',
    routes
});

export default router;
